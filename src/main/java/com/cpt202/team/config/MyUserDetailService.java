package com.cpt202.team.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cpt202.team.repositories.UserRepo;
import com.cpt202.team.security.MyUserDetails;
import java.util.Optional;
import com.cpt202.team.models.User;


@Service
public class MyUserDetailService implements UserDetailsService{

    @Autowired
    private UserRepo userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        
        // load user from JPA

        Optional<User> returnedUser = userRepo.findByUserName(username);
        User user = returnedUser.orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return new MyUserDetails(user);
    }
    
    
}
