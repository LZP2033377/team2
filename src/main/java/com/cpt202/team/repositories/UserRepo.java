package com.cpt202.team.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.cpt202.team.models.User;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Integer> {
    Optional<User> findByUserName(String userName);
}
